<?php
/*
Plugin Name: Popular Posts
Plugin URI: https://www.adm.ee
Description: Display popular posts.
Version: 1.0
Author: ADM
Author URI: https://www.adm.ee
*/

namespace ADM\PopularPosts {
    class PopularPosts
    {
        /**
         * PopularPosts constructor.
         */
        public function __construct()
        {
            $this->init();
        }

        /**
         * Init plugin.
         *
         * @return void
         */
        public function init()
        {
            // Track post views
            add_action('wp_head', [$this, 'blog_track_post_views']);

            // To keep the count accurate, lets get rid of prefetching
            remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

            // Add shortcode
            add_shortcode('adm_popular_posts', [$this, 'popular_posts_shortcode']);
        }

        /**
         * Track post view count.
         *
         * @param $post_id
         * @return void
         */
        public function blog_track_post_views($post_id)
        {
            if (!is_single()) {
                return;
            }

            if (empty($post_id)) {
                global $post;
                $post_id = $post->ID;
            }

            $this->blog_set_post_views($post_id);
        }

        /**
         * Set post view count.
         *
         * @param $post_id
         * @return void
         */
        public function blog_set_post_views($post_id)
        {
            if (current_user_can('edit_posts')) {
                return;
            }

            $count_key = 'post_views_count';
            $date_key = 'post_last_viewed';
            $count = get_post_meta($post_id, $count_key, true);

            if (!$count) {
                delete_post_meta($post_id, $count_key);
                delete_post_meta($post_id, $date_key);

                add_post_meta($post_id, $count_key, '1');
                add_post_meta($post_id, $date_key, date('d.m.Y H:i:s'));
            } else {
                $count++;
                update_post_meta($post_id, $count_key, $count);
                update_post_meta($post_id, $date_key, date('d.m.Y H:i:s'));
            }
        }

        /**
         * Shortcode for partials/popular.php.
         *
         * @return false|string
         */
        public function popular_posts_shortcode()
        {
            ob_start();
            include('partials/popular.php');
            $output = ob_get_contents();
            ob_end_clean();
            return $output;
        }
    }
}

namespace {
    new \ADM\PopularPosts\PopularPosts();
}