<?php
$popularpost = new WP_Query(
    array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'meta_query' =>  array(
            'relation' => 'AND',
            'views_count' => array(
                'key' => 'post_views_count',
                'compare' => 'EXISTS'
            ),
            'last_viewed' => array(
                'key' => 'post_last_viewed',
                'value' => date('d.m.Y H:i:s', strtotime('-14 day')),
                'compare' => '>',
            )
        ),
        'orderby' => array(
            'views_count' => 'DESC',
            'last_viewed' => 'DESC'
        ),
    )
);
?>

<?php if ($popularpost->have_posts()) : ?>
    <section class="article-section popular-articles">
        <div class="container">
            <div class="article-section__header">
                <h2><?= __('Popular Posts', 'popular_posts'); ?></h2>
            </div>
            <div class="row">
                <?php while ($popularpost->have_posts()) : $popularpost->the_post(); ?>
                    <div class="col-md-4 article-column">
                        <div class="article">
                            <div class="article__content">
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <div class="excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
<?php else: ?>
    No popular posts yet.
<?php endif; ?>