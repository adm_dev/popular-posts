# Popular posts #

This is a basic WordPress plugin that displays most popular posts.

### How does it work? ###

* Popular posts can be displayed with a shortcode **[adm_popular_posts]**
* Popularity is determined by view count. Every time someone visits a post, the view count increases by 1.
* View count only increases for non-admin visitors.
* Posts are displayed in order of popularity.
* By default, only 3 posts will be displayed.
* Posts must have been visited within the last 14 days to be displayed.
* If posts don't have any visits at all, nothing will be shown. At least 1 visit is necessary.

### Developer notes ###

* This is a very basic plugin, it does not have settings page or customizable options.
* The plugin is very easily customizable with some basic WordPress and PHP knowledge.
* You can customize the output of popular posts in /partials/popular.php (beware of updates).
* You can also grab the entire contents of /partials/popular.php and insert it anywhere in the theme. 

Original author: Mihhail Jaskov (mihhail.jaskov@adm.ee)